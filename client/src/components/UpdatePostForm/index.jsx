import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';

const EditPostForm = ({
  post,
  updateEditedPost,
  toggleEditedPost: toggle
}) => {
  const [body, setBody] = useState(post.body);
  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await updateEditedPost(post.id, body);
  };

  return (
    <Segment>
      <Form onSubmit={() => { handleEditPost(); toggle(); }}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder={body}
          onChange={ev => setBody(ev.target.value)}
        />
        <Button type="submit" content="Update post" icon="edit" primary />
      </Form>
    </Segment>
  );
};

EditPostForm.propTypes = {
  updateEditedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired
};

export default EditPostForm;
