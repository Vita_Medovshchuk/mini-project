import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Button, Form } from 'semantic-ui-react';
import moment from 'moment';

const Post = ({ post, deletePost, toggleDeletedPost: toggle }) => {
  const {
    image,
    body,
    user,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const handleDeletePost = async () => {
    await deletePost(post.id);
  };
  return (
    <Form reply onSubmit={() => { handleDeletePost(); toggle(); }}>
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          <Card.Description>
            {body}
          </Card.Description>
          <Button content="No" icon="delete" floated="right" secondary onClick={() => toggle()} />
          <Button type="submit" content="Yes" floated="right" icon="trash alternate" color="google plus" />
        </Card.Content>
      </Card>
    </Form>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired
};

export default Post;
