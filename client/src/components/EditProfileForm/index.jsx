import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input, Image, Icon, Segment } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';
import * as imageService from 'src/services/imageService';

const EditProfileForm = ({
  user,
  alreadyExistUserName,
  toggleEditedUser: toggle,
  updateUser: edit,
  toggleExistUsernameToUndefined: toggleExistUsername
}) => {
  const [userName, setUserName] = useState(user.username);
  const [isUploading, setIsUploading] = useState(false);
  const [userImage, setuserImage] = useState(undefined);
  const handleEditProfile = async () => {
    if (!userName) {
      return;
    }
    await edit(user.id, user.username, userName, userImage);
  };
  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setuserImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };
  return (
    <Segment>
      <Form onSubmit={() => { handleEditProfile(); toggle(); }}>
        <Image src={userImage?.imageLink || getUserImgLink(user.image)} alt="post" />
        <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
          <Icon name="image" />
          Attach image
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={userName}
          onChange={ev => { setUserName(ev.target.value); toggleExistUsername(); }}
        />
        {alreadyExistUserName
          ? (
            <>
              <span>this username already exists</span>
              <Button type="submit" content="Edit" labelPosition="left" icon="edit" disabled primary />
            </>
          )
          : (
            <>
              <Button type="submit" content="Edit" labelPosition="left" icon="edit" primary />
            </>
          )}
      </Form>
    </Segment>
  );
};

EditProfileForm.propTypes = {
  toggleEditedUser: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateUser: PropTypes.func.isRequired,
  alreadyExistUserName: PropTypes.string,
  toggleExistUsernameToUndefined: PropTypes.func.isRequired
};

EditProfileForm.defaultProps = {
  alreadyExistUserName: undefined
};

export default EditProfileForm;
