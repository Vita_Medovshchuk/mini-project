import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Grid, Popup, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Post = ({ post, dislikePost, userId, likePost, toggleDeletedPost, toggleEditedPost, getLikes, postLikes,
  toggleExpandedPost, sharePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const likeLabel = (
    <Label
      basic
      size="small"
      as="a"
      className={styles.toolbarBtn}
      onClick={() => likePost(id)}
    >
      <Icon name="thumbs up" />
      {likeCount}
    </Label>
  );

  const handleOpen = () => {
    getLikes(id);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          trigger={likeLabel}
          on="hover"
          onOpen={handleOpen}
        >
          {
            postLikes ? (
              <Grid
                columns={postLikes.length > 5 ? 3 : postLikes.length || 1}
              >
                {
                  postLikes.map(like => (
                    <Grid.Column key={like.id}>
                      <Image
                        src={getUserImgLink(like.user.image)}
                        avatar
                      />
                    </Grid.Column>
                  ))
                }
              </Grid>
            ) : ''
          }
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        <Form>
          {
            (userId === user.id.toString()) ? (
              <>
                <Button
                  type="submit"
                  content="Edit post"
                  icon="edit"
                  primary
                  floated="right"
                  onClick={() => toggleEditedPost(id)}
                />
                <Button
                  floated="right"
                  icon="trash alternate"
                  content="Delete Post"
                  color="google plus"
                  onClick={() => toggleDeletedPost(id)}
                />
              </>
            ) : null
          }
        </Form>
      </Card.Content>
    </Card>
  );
};

const mapStateToProps = rootState => ({
  postLikes: rootState.posts.postLikes
});

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  // eslint-disable-next-line react/require-default-props
  postLikes: PropTypes.arrayOf(PropTypes.object),
  userId: PropTypes.string.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  getLikes: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(Post);
