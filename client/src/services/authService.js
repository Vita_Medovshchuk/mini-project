import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const getUserByName = async userName => {
  try {
    const response = await callWebApi({
      endpoint: `/api/auth/user/${userName}`,
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUser = async (userId, newUserName, newImage) => {
  try {
    const response = await callWebApi({
      endpoint: `/api/auth/user/${userId}`,
      type: 'PUT',
      request: {
        userId,
        username: newUserName,
        imageId: newImage.imageId
      }
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

