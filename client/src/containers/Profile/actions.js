import * as authService from 'src/services/authService';
import { SET_USER, SET_EXIST_USER, SET_UPDATE_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setAlreadyExistUserName = userName => async dispatch => dispatch({
  type: SET_EXIST_USER,
  userName
});

const setUpdateUser = user => async dispatch => dispatch({
  type: SET_UPDATE_USER,
  user
});

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

export const updateUser = (userId, oldUserName, newUserName, avatar = '') => async (dispatch, getRootState) => {
  const id = await authService.getUserByName(newUserName);
  let isChanged;

  if (id && oldUserName !== newUserName) {
    setAlreadyExistUserName(newUserName)(dispatch, getRootState);
    isChanged = false;
  } else {
    await authService.updateUser(userId, newUserName, avatar);
    isChanged = true;
  }

  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
  return isChanged;
};

export const toggleEditedUser = () => async (dispatch, getRootState) => {
  const { profile: { editedUser } } = getRootState();
  const user = editedUser ? undefined : await authService.getCurrentUser();
  setUpdateUser(user)(dispatch, getRootState);
};

export const toggleExistUsernameToUndefined = () => (dispatch, getRootState) => {
  setAlreadyExistUserName(undefined)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};
