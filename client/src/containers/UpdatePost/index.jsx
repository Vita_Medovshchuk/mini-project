import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Header, Modal } from 'semantic-ui-react';
import EditPostForm from 'src/components/UpdatePostForm';
import { updateEditedPost, toggleEditedPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

const EditPost = ({
  post,
  toggleEditedPost: toggle,
  updateEditedPost: update
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Update Post
          </Header>
          <EditPostForm post={post} updateEditedPost={update} toggleEditedPost={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updateEditedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editedPost
});

const actions = { updateEditedPost, toggleEditedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);
