import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  SET_EDITED_POST,
  SET_DELETED_POST,
  SET_DISPLAY_POST_LIKES
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case SET_EDITED_POST:
      return {
        ...state,
        editedPost: action.post
      };
    case SET_DELETED_POST:
      return {
        ...state,
        deletedPost: action.post
      };
    case SET_DISPLAY_POST_LIKES:
      return {
        ...state,
        postLikes: action.postLikes
      };
    default:
      return state;
  }
};
