/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditPost from 'src/containers/UpdatePost';
import DeletePost from 'src/containers/DeletePost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadPosts, loadMorePosts, likePost, dislikePost, getUsersWhoLikesPost,
  toggleExpandedPost, toggleDeletedPost, deletePost, toggleEditedPost, addPost } from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  likedByMe: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editedPost,
  deletedPost,
  hasMorePosts,
  // eslint-disable-next-line no-shadow
  getUsersWhoLikesPost,
  addPost: createPost,
  likePost: like,
  toggleExpandedPost: toggle,
  toggleDeletedPost: toggleDeleted,
  deletePost: postDelete,
  toggleEditedPost: toggleEdited,
  dislikePost: dislike
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedByMe = () => {
    setShowLikedByMe(!showLikedByMe);
    postsFilter.likedByMe = showLikedByMe ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);
  const paddingCheckbox = {
    padding: 15
  };
  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          style={paddingCheckbox}
          toggle
          label="Show posts liked by me"
          checked={showLikedByMe}
          onChange={toggleShowLikedByMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggle}
            toggleDeletedPost={toggleDeleted}
            getLikes={getUsersWhoLikesPost}
            toggleEditedPost={toggleEdited}
            sharePost={sharePost}
            key={post.id}
            userId={userId}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editedPost && <EditPost sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {deletedPost && <DeletePost post={deletedPost} toggleDeletedPost={toggleDeleted} deletePost={postDelete} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editedPost: PropTypes.objectOf(PropTypes.any),
  deletedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  getUsersWhoLikesPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editedPost: undefined,
  userId: undefined,
  deletedPost: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editedPost: rootState.posts.editedPost,
  userId: rootState.profile.user.id,
  deletedPost: rootState.posts.deletedPost
});

const actions = {
  loadPosts,
  loadMorePosts,
  dislikePost,
  likePost,
  toggleExpandedPost,
  toggleEditedPost,
  toggleDeletedPost,
  deletePost,
  getUsersWhoLikesPost,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
