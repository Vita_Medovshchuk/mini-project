
module.exports = {
  up(queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.addColumn(
        'posts', // table name
        'deletedAt', // new field name
        {
          type: Sequelize.DATE,
          allowNull: true
        }
      )
    ]);
  },

  down(queryInterface) {
    return Promise.all([
      queryInterface.removeColumn('posts', 'deletedAt')
    ]);
  }
};
