import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image };
};

export const updateUserById = ({ userId, username, imageId }) => {
  const updatedUser = userRepository.updateById(userId, { username, imageId });
  return updatedUser;
};

export const getUserByUserName = async userName => userRepository.getByUsername(userName);
